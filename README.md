Hashlist with all known paths for a given update. 

Paths may disappear between updates if they are unused (and reappear if they are used again). 

The paths were determined with heuristics and some manual labor, see also: http://steamcommunity.com/groups/pd2mech/discussions/0/333656722983521183/
